package br.com.team.maia.hackathon.controller;

import br.com.team.maia.hackathon.model.Aluno;
import br.com.team.maia.hackathon.service.api.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping( value = "/aluno" )
public class AlunoController {

    private static final String PATH_ID = "/{id}";

    @Autowired
    private AlunoService alunoService;

    public AlunoController(AlunoService alunoService) {
        this.alunoService = alunoService;
    }

    @ResponseBody
    @RequestMapping( method = RequestMethod.GET )
    public Iterable<Aluno> findAll() {
        Iterable<Aluno> list = alunoService.findAll();
        return list;
    }

    @ResponseBody
    @RequestMapping( method = RequestMethod.POST )
    public Aluno insert(@RequestBody Aluno aluno) {
        return alunoService.insert(aluno);
    }

    @ResponseBody
    @RequestMapping( value = PATH_ID, method = RequestMethod.GET )
    public Aluno search(@PathVariable Integer id ) {
        return alunoService.search( id );
    }

    @ResponseBody
    @RequestMapping( method = RequestMethod.PUT )
    public Aluno update(@RequestBody Aluno aluno) {
        return alunoService.update(aluno);
    }

    @ResponseBody
    @RequestMapping( value = PATH_ID, method = RequestMethod.DELETE )
    public void delete( @PathVariable Integer id ) {
        alunoService.delete( id );
    }

}