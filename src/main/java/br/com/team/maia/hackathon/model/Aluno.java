package br.com.team.maia.hackathon.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table( name = "aluno" )
public class Aluno {

    @Id
    @Column( name = "id_aluno" )
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer idAluno;

    @Column( name = "ds_nome" )
    private String dsNome;

    @Column( name = "nr_idade" )
    private Integer nrIdade;

    @Column( name = "ds_sexo" )
    private String dsSexo;

    @Column( name = "ds_estado" )
    private String dsEstado;

    @Column( name = "ds_cidade" )
    private String dsCidade;

    //--- Caracteristicas

    @Column( name = "hr_lanche" )
    private String hrLanche;

    @Column( name = "hr_banheiro" )
    private String hrBanheiro;

    @Column( name = "hr_agua" )
    private String hrAgua;

    //--- Habilidades

    @Column( name = "nv_locomacao" )
    private Integer nvLocomocao;

    @Column( name = "nv_fala" )
    private Integer nvFala;

    @Column( name = "nv_escrita" )
    private Integer nvEscrita;

    @Column( name = "nv_raciocinio" )
    private Integer nvRaciocinio;

    @Column( name = "nv_compreensao" )
    private Integer nvCompreensao;

}
