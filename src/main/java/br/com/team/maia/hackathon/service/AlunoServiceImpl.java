package br.com.team.maia.hackathon.service;


import br.com.team.maia.hackathon.model.Aluno;
import br.com.team.maia.hackathon.repository.AlunoRepository;
import br.com.team.maia.hackathon.service.api.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by kielsonzinn on 19/05/17.
 */
@Service
public class AlunoServiceImpl implements AlunoService {

    @Autowired
    private AlunoRepository alunoRepository;

    @Override
    public Iterable<Aluno> findAll() {
        return alunoRepository.findAll();
    }

    @Override
    public Aluno insert(Aluno aluno) {
        return alunoRepository.save(aluno);
    }

    @Override
    public Aluno search(Integer id) {
        return alunoRepository.findOne(id);
    }

    @Override
    public Aluno update(Aluno aluno) {
        return alunoRepository.save(aluno);
    }

    @Override
    public void delete(Integer id) {
        alunoRepository.delete( id );
    }

}
