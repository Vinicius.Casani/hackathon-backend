define(function(require) {
    'use strict';

    var module = require('./module');

    module.factory('AlunoResource', AlunoResource);

    //---

    AlunoResource.$inject = ['$resource', 'BACKEND_API_URL_ROOT'];

    function AlunoResource($resource, rootUrl) {

        return $resource( rootUrl + '/aluno/:id', {
        });

    }

});
