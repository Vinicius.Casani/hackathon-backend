define(function(require) {
  'use strict';

  var module = require('./module');

  module.controller('MainCtrl', MainCtrl);

  //---

  MainCtrl.$inject = [];

  function MainCtrl() {
    var vm = this;

    vm.getAgua = getAguaFn;
    vm.getComida = getComidaFn;
    vm.getBanheiro = getBanheiroFn;

    function getAguaFn() {
        var msg = new SpeechSynthesisUtterance("Estou com sêdê!");
        msg.pitch = 1;
        msg.lang = 'pt-BR';
        window.speechSynthesis.speak(msg);
    }

    function getComidaFn() {
        var msg = new SpeechSynthesisUtterance("Estou com fome!");
        msg.pitch = 1;
        msg.lang = 'pt-BR';
        window.speechSynthesis.speak(msg);
    }

    function getBanheiroFn() {
        var msg = new SpeechSynthesisUtterance("Preciso ir ao banheiro!");
        msg.pitch = 1;
        msg.lang = 'pt-BR';
        window.speechSynthesis.speak(msg);
    }

  }

});
