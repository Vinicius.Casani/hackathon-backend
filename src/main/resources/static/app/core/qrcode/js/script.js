var object = undefined;
var QR_CODES = [ "VALOR01", "VALOR02", "VALOR03" ];
var CODE_VALUES = [ "Quadrado", "Circulo", "Triângulo" ];
var vitorias = 0;

function playAudio( msg ) {
    var msg = new SpeechSynthesisUtterance( msg );
    msg.pitch = 1;
    msg.lang = 'pt-BR';

    window.speechSynthesis.speak(msg);
}

function getRandomInt( min, max ) {
    return Math.floor( Math.random() * ( max - min + 1 ) + min );
}

function onSort() {
    console.log( "mensagem");
    var sortInt = getRandomInt( 0, 2 );

    if ( QR_CODES[sortInt] == object ) {
        onSort();
        return;
    }

    object = QR_CODES[sortInt];

    var value = CODE_VALUES[sortInt];
    document.getElementById('divValue').innerHTML = 'Qual objeto é um ' + value + '?';

    playAudio( "Qual objeto é um" + value + "?" );
}

let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
scanner.addListener('scan', function (content) {
    if ( content == object ) {
        playAudio( 'Parabéns, você acertou!' );
        vitorias++;

        if ( vitorias < 3 ) {
            onSort();
        } else {
            $('#qrcode').css("display", "none");
            $('#ending').css("display", "block");
            $('body').css("background", "#222");
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'fireworks.js';

            document.getElementsByTagName('head')[0].appendChild(script);

            setTimeout(function(){ window.history.back(); }, 5000);
        }
    } else {
        playAudio( 'Tente outro objeto.' );
    }
});

Instascan.Camera.getCameras().then(function (cameras) {
    if (cameras.length > 0) {
        scanner.start(cameras[0]);
    } else {
        console.error('No cameras found.');
    }
}).catch(function (e) {
    console.error(e);
});

onSort();