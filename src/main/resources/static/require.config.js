require({

  paths: {

    angular: [
      'assets/vendor/angular/angular'
    ],

    angularResource: [
      'assets/vendor/angular-resource/angular-resource.min'
    ],

    angularAnimate: [
      'assets/vendor/angular-animate/angular-animate.min'
    ],

    angularAria: [
      'assets/vendor/angular-aria/angular-aria.min'
    ],

    angularCookies: [
      'assets/vendor/angular-cookies/angular-cookies.min'
    ],

    uiRouter: [
      'assets/vendor/angular-ui-router/angular-ui-router.min'
    ]

  },

  shim: {

    'angular': {
      exports: 'angular'
    },

    'angularResource': {
      deps: ['angular']
    },

    'angularAnimate': {
      deps: ['angular']
    },

    'angularAria': {
      deps: ['angular']
    },

    'angularCookies': {
      deps: ['angular']
    },

    'uiRouter': {
      deps: ['angular']
    }

  },

  priority: [
    'angular'
  ],

  deps: ['./ng.app']

});
