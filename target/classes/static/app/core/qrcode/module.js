define(function(require) {
  'use strict';

  var angular = require('angular');

  require('uiRouter');
  
  return angular.module(
    'qrcode',
      
    [
      'ui.router'
    ]
  );

});
