define(function(require) {
  'use strict';

  var angular = require('angular');
  require('uiRouter');

  return angular.module(
    'core',

    [
      'ui.router',

      require('./layout/package').name,
      require('./main/package').name,
      require('./aluno/package').name,
      require('./qrcode/package').name

    ]
  );

});
