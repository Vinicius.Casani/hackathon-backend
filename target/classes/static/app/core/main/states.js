define(function(require) {
  'use strict';

  var module = require('./module');

  module.config(configureStates);

  //---

  configureStates.$inject = [ '$urlRouterProvider', '$stateProvider' ];

  function configureStates( $urlRouterProvider, $stateProvider ) {

    $urlRouterProvider
      .when('', '/home')
      .when('/', '/home')
      .otherwise("/home");
  }

});
