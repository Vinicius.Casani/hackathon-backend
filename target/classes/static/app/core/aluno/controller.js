define(function(require) {
  'use strict';

  var module = require('./module');

  module.controller('AlunoCtrl', AlunoCtrl);

  //---

  AlunoCtrl.$inject = ['$scope'];

  function AlunoCtrl($scope) {
    
    $scope.model = {

    };

    $scope.salvar = salvarFn;
    $scope.buscar = buscarFn;
    $scope.showInfos = showInfosFn;
    $scope.showHabilidades = showHabilidadesFn;
    $scope.showCaracteristicas = showCaracteristicasFn;

    $scope.habilidades = false;
    $scope.infos = true;
    $scope.caracteristicas = false;

    function buscarFn(){

      console.log($scope.model);
      $http.get("http://172.24.255.167:8080/aluno").then(function(response) {
        $scope.myWelcome = response.data;
      });
    }

    function salvarFn(){
      console.log($scope.model);
      $http.post("http://172.24.255.167:8080/aluno",  $scope.model).then(function(response) {
        $scope.myWelcome = response.data;
      });
    }

    function showInfosFn() {
      $scope.habilidades = false;
      $scope.infos = true;
      $scope.caracteristicas = false;
    }

    function showHabilidadesFn() {
      $scope.habilidades = true;
      $scope.infos = false;
      $scope.caracteristicas = false;
    }

    function showCaracteristicasFn() {
      $scope.habilidades = false;
      $scope.infos = false;
      $scope.caracteristicas = true;
    }

  }

});
